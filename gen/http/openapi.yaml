swagger: "2.0"
info:
    title: Information Hub Service
    description: Information Hub Service exposes HTTP API for exporting and importing information.
    version: ""
host: localhost:8084
consumes:
    - application/json
    - application/xml
    - application/gob
produces:
    - application/json
    - application/xml
    - application/gob
paths:
    /liveness:
        get:
            tags:
                - health
            summary: Liveness health
            operationId: health#Liveness
            responses:
                "200":
                    description: OK response.
                    schema:
                        $ref: '#/definitions/HealthLivenessResponseBody'
                        required:
                            - service
                            - status
                            - version
            schemes:
                - http
    /readiness:
        get:
            tags:
                - health
            summary: Readiness health
            operationId: health#Readiness
            responses:
                "200":
                    description: OK response.
                    schema:
                        $ref: '#/definitions/HealthReadinessResponseBody'
                        required:
                            - service
                            - status
                            - version
            schemes:
                - http
    /v1/export/{exportName}:
        get:
            tags:
                - infohub
            summary: Export infohub
            description: Export returns data signed as Verifiable Presentation.
            operationId: infohub#Export
            parameters:
                - name: exportName
                  in: path
                  description: Name of export to be performed.
                  required: true
                  type: string
            responses:
                "200":
                    description: OK response.
                    schema:
                        type: string
                        format: binary
            schemes:
                - http
    /v1/import:
        post:
            tags:
                - infohub
            summary: Import infohub
            description: Import the given data wrapped as Verifiable Presentation into the Cache.
            operationId: infohub#Import
            parameters:
                - name: bytes
                  in: body
                  description: Data wrapped in Verifiable Presentation that will be imported into Cache.
                  required: true
                  schema:
                    type: string
                    format: byte
            responses:
                "200":
                    description: OK response.
                    schema:
                        $ref: '#/definitions/InfohubImportResponseBody'
                        required:
                            - importIds
            schemes:
                - http
definitions:
    HealthLivenessResponseBody:
        title: HealthLivenessResponseBody
        type: object
        properties:
            service:
                type: string
                description: Service name.
                example: Atque voluptatem.
            status:
                type: string
                description: Status message.
                example: Optio et enim in eum.
            version:
                type: string
                description: Service runtime version.
                example: A consequatur iusto.
        example:
            service: Vel voluptates.
            status: Ut quidem.
            version: Iusto reprehenderit praesentium sint est molestiae labore.
        required:
            - service
            - status
            - version
    HealthReadinessResponseBody:
        title: HealthReadinessResponseBody
        type: object
        properties:
            service:
                type: string
                description: Service name.
                example: Placeat quia qui tenetur.
            status:
                type: string
                description: Status message.
                example: Est adipisci incidunt.
            version:
                type: string
                description: Service runtime version.
                example: Ipsa cum expedita dolore.
        example:
            service: Excepturi iusto.
            status: Ut quo quae.
            version: Qui consequatur laborum et dolorem.
        required:
            - service
            - status
            - version
    InfohubImportResponseBody:
        title: InfohubImportResponseBody
        type: object
        properties:
            importIds:
                type: array
                items:
                    type: string
                    example: Rerum possimus dolor fugiat fugit.
                description: importIds is an array of unique identifiers used as Cache keys to retrieve the imported data entries later.
                example:
                    - 585a999a-f36d-419d-bed3-8ebfa5bb79c9
        example:
            importIds:
                - 585a999a-f36d-419d-bed3-8ebfa5bb79c9
        required:
            - importIds
